//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.collection.SimpleArrayMap;


public class DualPaneContainer extends LinearLayoutCompat implements Container {

    private static final String LOG_TAG = "Elektrichki.DPC";

    public DualPaneContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(LOG_TAG, "inflated");
    }

    @Override
    public void showSchedule(SimpleArrayMap<String, String> ab) {
        Log.d(LOG_TAG, "show " + ab.get(Const.DESR) + " to " + ab.get(Const.AESR));
        ScheduleView scheduleView = findViewById(R.id.view_schedule);
        SchedulePresenter.makeRequest(scheduleView, ab.get(Const.DESR), ab.get(Const.AESR));
    }

}
