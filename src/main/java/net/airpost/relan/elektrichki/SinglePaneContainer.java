//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;
import androidx.collection.SimpleArrayMap;


public class SinglePaneContainer extends FrameLayout implements Container {

    private static final String LOG_TAG = "Elektrichki.SPC";

    public SinglePaneContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(LOG_TAG, "inflated");
    }

    @Override
    public void showSchedule(SimpleArrayMap<String, String> ab) {
        Intent intent = new Intent(getContext(), ScheduleActivity.class);

        Log.d(LOG_TAG, "show " + ab.get(Const.DESR) + " to " + ab.get(Const.AESR));
        intent.putExtra(Const.DSTATION, ab.get(Const.DSTATION));
        intent.putExtra(Const.ASTATION, ab.get(Const.ASTATION));
        intent.putExtra(Const.DESR, ab.get(Const.DESR));
        intent.putExtra(Const.AESR, ab.get(Const.AESR));
        getContext().startActivity(intent);
    }

}
