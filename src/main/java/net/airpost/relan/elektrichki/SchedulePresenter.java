//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class SchedulePresenter {

    private static final String LOG_TAG = "Elektrichki.SP";

    public static void makeRequest(ScheduleView view, String desr, String aesr) {
        // Do an asynchronous request
        Task task = new Task(view);
        task.execute(desr, aesr);
    }

    static class Task extends FetchTask {

        private final WeakReference<ScheduleView> SCHEDULE_VIEW_REF;

        Task(ScheduleView view) {
            super(view.getContext());
            SCHEDULE_VIEW_REF = new WeakReference(view);
        }

        @Override
        protected void onProgressUpdate(Object... result) {
            ScheduleView view = SCHEDULE_VIEW_REF.get();
            if (view == null)
                return;

            view.appendJourneys((ArrayList<Journey>) result[0]);
        }

        @Override
        protected void onPostExecute(Object result) {
            ScheduleView view = SCHEDULE_VIEW_REF.get();

            if (view == null)
                return;

            // On error we get an Integer
            if (result instanceof Integer) {
                view.showError((Integer) result);
                return;
            }

            // On success we get Journeys
            view.replaceJourneys((ArrayList<Journey>) result);
        }
    }

}
