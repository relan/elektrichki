//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Adapter extends ArrayAdapter<Journey> {

    private static final String LOG_TAG = "Elektrichki.A";

    private final Context CONTEXT;
    private List<Journey> mList;
    private final SimpleDateFormat HM;

    public Adapter(Context context, int resource, int textViewResourceId,
            List<Journey> objects) {
        super(context, resource, textViewResourceId, objects);

        CONTEXT = context;
        mList = objects;
        HM = new SimpleDateFormat("H:mm", Locale.getDefault());
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflate(parent);

        Holder holder = (Holder) convertView.getTag();
        Journey journey = mList.get(position);
        Date now = new Date();
        boolean relevant = journey.isRelevant(now);

        actualize(holder.mDTimeTextView, relevant,
                HM.format(journey.getDTime()));
        actualize(holder.mATimeTextView, relevant,
                HM.format(journey.getATime()));
        actualize(holder.mDurationTextView, relevant,
                getQString(R.plurals.N_minutes, journey.getDurationMinutes()));
        actualize(holder.mDPlatformTextView, relevant,
                journey.getDPlatform());
        actualize(holder.mAPlatformTextView, relevant,
                journey.getAPlatform());
        actualize(holder.mTickTockTextView, relevant,
                getTickTockText(now, position));
        actualize(holder.mRouteTextView, relevant,
                journey.getRoute());

        return convertView;
    }

    public View inflate(ViewGroup parent) {
        View convertView = ((LayoutInflater) CONTEXT.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.row_journey, parent, false);
        convertView.setTag(new Holder(convertView));
        return convertView;
    }

    public int getNearest() {
        Date now = new Date();
        for (int i = 0; i < mList.size(); i++)
            if (mList.get(i).isRelevant(now))
                return i;
        return mList.size() - 1;
    }

    private boolean isNearest(int position, Date moment) {
        // This journey is relevant and previous is not.
        return mList.get(position).isRelevant(moment) &&
                (position == 0 || !mList.get(position - 1).isRelevant(moment));
    }

    private void actualize(TextView view, boolean relevant, String text) {
        view.setText(text);
        view.setEnabled(relevant);
        view.setVisibility(text.isEmpty() ? View.GONE : View.VISIBLE);
    }

    private String getTickTockText(Date moment, int position) {
        int minutes = mList.get(position).getMinutesToDeparture(moment);

        // Show tick-tock label if this journey will happen within half
        // an hour or is the nearest one.
        if (minutes < 0)
            return "";
        if (minutes > 30 && !isNearest(position, moment))
            return "";

        // Special case for "0 minutes"
        if (minutes == 0)
            return getString(R.string.right_now);
        // "in N minutes"
        if (minutes < 60)
            return getQString(R.plurals.in_N_minutes, minutes);
        // "in M hours N minutes"
        String fmt = getString(R.string.in_M_hours_and_N_minutes);
        String h = getQString(R.plurals.in_M_hours, minutes / 60);
        String m = minutes % 60 == 0 ?
                getString(R.string.sharp) :
                getQString(R.plurals.and_N_minutes, minutes % 60);
        return String.format(fmt, h, m);
    }

    private String getString(int resourceId) {
        return CONTEXT.getResources().getString(resourceId);
    }

    private String getQString(int resourceId, int value) {
        return CONTEXT.getResources().getQuantityString(resourceId,
                value, value);
    }

    private static class Holder {

        public TextView mTickTockTextView;
        public TextView mDTimeTextView;
        public TextView mATimeTextView;
        public TextView mDurationTextView;
        public TextView mDPlatformTextView;
        public TextView mAPlatformTextView;
        public TextView mRouteTextView;

        Holder(View v) {
            mTickTockTextView = (TextView) v.findViewById(R.id.text_tick_tock);
            mDTimeTextView = (TextView) v.findViewById(R.id.text_dtime);
            mATimeTextView = (TextView) v.findViewById(R.id.text_atime);
            mDurationTextView = (TextView) v.findViewById(R.id.text_duration);
            mDPlatformTextView = (TextView) v.findViewById(R.id.text_dplatform);
            mAPlatformTextView = (TextView) v.findViewById(R.id.text_aplatform);
            mRouteTextView = (TextView) v.findViewById(R.id.text_route);
        }
    }
}
