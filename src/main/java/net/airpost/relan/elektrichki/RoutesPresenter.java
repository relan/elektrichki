//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.content.res.Resources;
import android.text.Selection;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import androidx.collection.SimpleArrayMap;


public class RoutesPresenter {

    private static final String LOG_TAG = "Elektrichki.RP";

    public static void show(Container container, Resources res,
            String departureStation, String arrivalStation) {
        String desr = getStationEsr(res, departureStation);
        String aesr = getStationEsr(res, arrivalStation);

        Log.d(LOG_TAG, "show " + desr + " to " + aesr);

        if (desr == null || aesr == null)
            return;

        SimpleArrayMap<String, String> ab = new SimpleArrayMap();
        ab.put(Const.DSTATION, departureStation);
        ab.put(Const.ASTATION, arrivalStation);
        ab.put(Const.DESR, desr);
        ab.put(Const.AESR, aesr);
        container.showSchedule(ab);
    }

    public static void swap(AutoCompleteTextView a, AutoCompleteTextView b,
            View focusedView) {
        if (focusedView == a)
            b.requestFocus();
        else if (focusedView == b)
            a.requestFocus();

        swapAutoCompleteTextView(a, b);
    }

    private static void swapAutoCompleteTextView(AutoCompleteTextView a,
            AutoCompleteTextView b) {
        float dy = b.getTop() - a.getTop();
        CharSequence textA = a.getText();
        CharSequence textB = b.getText();

        moveAutoCompleteTextView(a, +dy, textB);
        moveAutoCompleteTextView(b, -dy, textA);
    }

    private static void moveAutoCompleteTextView(AutoCompleteTextView view,
            float dy, CharSequence text) {
        view.setTranslationY(dy);
        view.setText(text);
        view.animate().translationY(0);

        view.setSelection(
                Selection.getSelectionStart(text),
                Selection.getSelectionEnd(text));
        view.dismissDropDown();
    }

    private static String getStationEsr(Resources res, String stationName) {
        String stations[] = res.getStringArray(R.array.stations_array);
        String esr[] = res.getStringArray(R.array.esr_array);

        for (int i = 0; i < stations.length; i++)
            if (stations[i].equals(stationName)) {
                return esr[i];
            }

        return null;
    }

}
