//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.util.Log;
import java.util.Date;


public class Journey {

    private static final String LOG_TAG = "Elektrichki.J";

    private final Date mDTime;
    private final Date mATime;
    private final String mDPlatform;
    private final String mAPlatform;
    private final String mRoute;

    public Journey(Date dtime, Date atime, String dplatform, String aplatform,
            String route) {
        mDTime = dtime;
        mATime = atime;
        mDPlatform = dplatform;
        mAPlatform = aplatform;
        mRoute = route;
    }

    public final Date getDTime() {
        return mDTime;
    }

    public final Date getATime() {
        return mATime;
    }

    public final String getDPlatform() {
        return mDPlatform;
    }

    public final String getAPlatform() {
        return mAPlatform;
    }

    public final String getRoute() {
        return mRoute;
    }

    public int getDurationMinutes() {
        // Journey duration in minutes
        return millisToMinutes(mATime.getTime() - mDTime.getTime());
    }

    public boolean isRelevant(Date moment) {
        return getMinutesToDeparture(moment) >= 0;
    }

    public int getMinutesToDeparture(Date moment) {
        return millisToMinutes(mDTime.getTime()) -
               millisToMinutes(moment.getTime());
    }

    public boolean equals(Object that) {
        return that instanceof Journey &&
                ((Journey) that).mDTime.equals(mDTime) &&
                ((Journey) that).mATime.equals(mATime) &&
                ((Journey) that).mDPlatform.equals(mDPlatform) &&
                ((Journey) that).mAPlatform.equals(mAPlatform) &&
                ((Journey) that).mRoute.equals(mRoute);
    }

    private int millisToMinutes(long millis) {
        return (int) (millis / 60000);
    }
}
