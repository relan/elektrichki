//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import java.util.Arrays;


public class RoutesView extends LinearLayoutCompat {

    private static final String LOG_TAG = "Elektrichki.RV";

    private class StationTextValidator implements TextWatcher {

        protected final String STATIONS[];
        protected boolean mIsValid;

        public StationTextValidator() {
            STATIONS = getResources().getStringArray(R.array.stations_array);
            mIsValid = false;
        }

        public void onValid() {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (Arrays.stream(STATIONS).anyMatch(
                    station -> station.equals(s.toString()))) {
                onValid();
                mIsValid = true;
                return;
            }
            mShowScheduleButton.setEnabled(false);
            mIsValid = false;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

    }

    private AutoCompleteTextView mDStationTextView;
    private AutoCompleteTextView mAStationTextView;
    private StationTextValidator mDStationTextValidator;
    private StationTextValidator mAStationTextValidator;
    private Button mShowScheduleButton;

    public RoutesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(LOG_TAG, "inflated");

        mDStationTextView = findViewById(R.id.autocomplete_dstation);
        mAStationTextView = findViewById(R.id.autocomplete_astation);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.stations_array));
        mDStationTextView.setAdapter(adapter);
        mAStationTextView.setAdapter(adapter);

        mShowScheduleButton = findViewById(R.id.button_show);
        mShowScheduleButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(LOG_TAG, "click");

                        final String DSTATION =
                                mDStationTextView.getText().toString();
                        final String ASTATION =
                                mAStationTextView.getText().toString();

                        // Save stations persistently
                        SharedPreferences.Editor editor =
                                getContext().getSharedPreferences(Const.SHARED_PREF,
                                        Context.MODE_PRIVATE).edit();
                        editor.putString(Const.DSTATION, DSTATION);
                        editor.putString(Const.ASTATION, ASTATION);
                        editor.apply();

                        RoutesPresenter.show(
                                ((MainActivity) getContext()).getContainer(),
                                getResources(), DSTATION, ASTATION);
                    }});

        mDStationTextValidator = new StationTextValidator() {
                @Override
                public void onValid() {
                    Log.d(LOG_TAG, "departure station is valid");
                    mShowScheduleButton.setEnabled(mAStationTextValidator.mIsValid);
                }
            };
        mDStationTextView.addTextChangedListener(mDStationTextValidator);

        mAStationTextValidator = new StationTextValidator() {
                @Override
                public void onValid() {
                    Log.d(LOG_TAG, "arrival station is valid");
                    mShowScheduleButton.setEnabled(mDStationTextValidator.mIsValid);
                }
            };
        mAStationTextView.addTextChangedListener(mAStationTextValidator);

        // Restore stations and manually trigger text validators to enable
        // the "Show schedule" button
        SharedPreferences pref = getContext().getSharedPreferences(
                Const.SHARED_PREF, Context.MODE_PRIVATE);
        mDStationTextView.setText(pref.getString(Const.DSTATION, ""));
        mDStationTextValidator.afterTextChanged(mDStationTextView.getText());
        mAStationTextView.setText(pref.getString(Const.ASTATION, ""));
        mAStationTextValidator.afterTextChanged(mAStationTextView.getText());

        ((ImageButton) findViewById(R.id.button_swap)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RoutesPresenter.swap(mDStationTextView,
                                mAStationTextView,
                                ((MainActivity) getContext()).getWindow().getCurrentFocus());
                    }});

    }

}
