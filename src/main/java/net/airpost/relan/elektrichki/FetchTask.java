//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


public class FetchTask extends AsyncTask<String, Object, Object> {

    private static final String LOG_TAG = "Elektrichki.FT";

    private final File CACHE_DIR;
    private final SimpleDateFormat YYYYMMDD;
    private final SimpleDateFormat YMDHM;
    private final String URL_TEMPLATE =
            "http://mobile.rasp.yandex.net" +
            "/export/suburban/trip/%s/%s/" +
            "?date=%s" +
            "&tomorrow_upto=3" +
            "&uuid=ecc5c20efedebb2565c104b6b19466fd";
    private final String ROUTE_TEMPLATE;
    private final String ROUTE_SEPARATOR = " — ";

    static public class EmptyScheduleException extends Exception {}

    static public class HttpResponseException extends Exception {}

    public FetchTask(Context context) {
        CACHE_DIR = context.getCacheDir();
        YYYYMMDD = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        YMDHM = new SimpleDateFormat("y-M-d H:m", Locale.US);
        ROUTE_TEMPLATE = context.getResources().getString(R.string.from_to);
    }

    @Override
    protected Object doInBackground(String... esrs) {
        try {
            clean();
            return fetch(esrs[0], esrs[1]);
        } catch (EmptyScheduleException e) {
            Log.e(LOG_TAG, "empty schedule error");
            return Integer.valueOf(R.string.empty_error);
        } catch (HttpResponseException e) {
            Log.e(LOG_TAG, "network error");
            return Integer.valueOf(R.string.network_error);
        } catch (SocketTimeoutException e) {
            Log.e(LOG_TAG, "connection timeout error");
            return Integer.valueOf(R.string.connection_error);
        } catch (UnknownHostException e) {
            Log.e(LOG_TAG, "unknown host error");
            return Integer.valueOf(R.string.connection_error);
        } catch (XmlPullParserException e) {
            Log.e(LOG_TAG, "XML error");
            return Integer.valueOf(R.string.xml_error);
        } catch (IOException e) {
            Log.e(LOG_TAG, "I/O error");
            return Integer.valueOf(R.string.io_error);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "parse error");
            return Integer.valueOf(R.string.parse_error);
        }
    }

    private void clean() {
        for (File cache : CACHE_DIR.listFiles()) {
            if (cache.getName().endsWith(".xml") && isObsolete(cache)) {
                if (cache.delete()) {
                    Log.d(LOG_TAG, "deleted " + cache.getName());
                } else {
                    Log.d(LOG_TAG, "failed to delete " + cache.getName());
                }
            }
        }
    }

    private boolean isObsolete(File cache) {
        final long DAY = 24 * 60 * 60 * 1000; // In milliseconds
        final long LIFETIME = 27 * 60 * 60 * 1000; // In milliseconds

        return (System.currentTimeMillis() - cache.lastModified() / DAY * DAY) > LIFETIME;
    }

    private ArrayList<Journey> fetch(String desr, String aesr) throws
            EmptyScheduleException,
            HttpResponseException,
            XmlPullParserException,
            IOException,
            ParseException {
        CacheEntry entry = new CacheEntry(CACHE_DIR, desr, aesr);

        if (entry.isOutdated()) {
            Log.d(LOG_TAG, "cache entry " + desr + "-" + aesr + " is outdated");
            // Show cached info if it exists.
            try {
                publishProgress(parse(entry));
                Log.d(LOG_TAG, "took schedule from cache");
            } catch (IOException e) {
                // Do nothing. Cache entry probably just does not exist which
                // is perfectly OK.
            }
            entry.refresh();
        } else {
            Log.d(LOG_TAG, "cache entry " + desr + "-" + aesr + " is up to date");
        }

        return parse(entry);
    }

    private ArrayList<Journey> parse(CacheEntry entry) throws
            EmptyScheduleException,
            XmlPullParserException,
            IOException,
            ParseException {
        // TODO Use try-with-resources after minSdkVersion becomes 19+
        InputStream input = null;
        try {
            input = entry.getInputStream();
            return parse(input);
        } finally {
            if (input != null)
                input.close();
        }
    }

    private ArrayList<Journey> parse(InputStream input) throws
            EmptyScheduleException,
            XmlPullParserException,
            IOException,
            ParseException {
        ArrayList<Journey> schedule = new ArrayList<Journey>();
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(input, null);

        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;
            if (!parser.getName().equals("segment"))
                continue;
            String departure = parser.getAttributeValue("", "departure");
            String arrival = parser.getAttributeValue("", "arrival");
            Log.d(LOG_TAG, departure + " ... " + arrival);
            String route = parser.getAttributeValue("", "title");
            int sep = route.indexOf(ROUTE_SEPARATOR);
            schedule.add(new Journey(
                    YMDHM.parse(departure),
                    YMDHM.parse(arrival),
                    parser.getAttributeValue("", "departure_platform"),
                    parser.getAttributeValue("", "arrival_platform"),
                    String.format(ROUTE_TEMPLATE,
                            route.substring(0, sep),
                            route.substring(sep + ROUTE_SEPARATOR.length()))));
        }

        if (schedule.isEmpty())
            throw new EmptyScheduleException();
        return schedule;
    }

    private class CacheEntry {

        private final URL LINK;
        private final File TEMPORARY_FILE;
        private final File CACHE_FILE;
        private final long TIMEOUT = 5 * 60 * 1000; // In milliseconds

        CacheEntry(File cacheDirectory, String desr, String aesr) throws
                MalformedURLException {
            // TODO Use Moscow time zone
            LINK = new URL(String.format(URL_TEMPLATE,
                    desr, aesr, YYYYMMDD.format(new Date())));
            TEMPORARY_FILE = new File(cacheDirectory, "temporary.xml");
            CACHE_FILE = new File(cacheDirectory, desr + "-" + aesr + ".xml");
        }

        public InputStream getInputStream() throws FileNotFoundException {
            return new BufferedInputStream(new FileInputStream(CACHE_FILE));
        }

        public boolean isOutdated() {
            return CACHE_FILE.lastModified() < System.currentTimeMillis() - TIMEOUT;
        }

        public void refresh() throws HttpResponseException, IOException {
            Log.d(LOG_TAG, "downloading " + LINK.toString());

            HttpURLConnection connection = (HttpURLConnection) LINK.openConnection();
            connection.setConnectTimeout(30000); // In milliseconds
            connection.setReadTimeout(30000);    // In milliseconds
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            connection.connect();
            Log.d(LOG_TAG, "HTTP response is " + connection.getResponseCode());
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new HttpResponseException();

            // TODO Use try-with-resources after minSdkVersion becomes 19+
            BufferedInputStream input = null;
            try {
                input = new BufferedInputStream(connection.getInputStream());
                save(input);
            } finally {
                if (input != null)
                    input.close();
            }
        }

        private void save(InputStream input) throws IOException {
            // TODO Use try-with-resources after minSdkVersion becomes 19+
            BufferedOutputStream output = null;
            try {
                output = new BufferedOutputStream(new FileOutputStream(TEMPORARY_FILE));
                byte[] buffer = new byte[0x2000];
                int length;

                while ((length = input.read(buffer)) >= 0)
                    output.write(buffer, 0, length);
            } finally {
                if (output != null)
                    output.close();
            }
            if (!TEMPORARY_FILE.renameTo(CACHE_FILE))
                throw new IOException();
        }
    }
}
