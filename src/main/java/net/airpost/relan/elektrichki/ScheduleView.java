//
// Elektrichki: suburban trains schedule for Android
// Copyright (C) 2015-2023  Andrew Nayenko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package net.airpost.relan.elektrichki;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;


public class ScheduleView extends RelativeLayout {

    private static final String LOG_TAG = "Elektrichki.SV";

    private ArrayList<Journey> mSchedule;
    private Adapter mAdapter;

    private ProgressBar mProgressBar;
    private ListView mListView;
    // TODO Make the error layout a custom view
    private ViewGroup mErrorLayout;
    private TextView mErrorTextView;

    public ScheduleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(LOG_TAG, "inflated");

        mSchedule = new ArrayList<Journey>();
        mAdapter = new Adapter(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1,
                mSchedule);
        mProgressBar = findViewById(R.id.progress);
        mListView = findViewById(R.id.list_schedule);
        mErrorLayout = findViewById(R.id.layout_error);
        mErrorTextView = findViewById(R.id.text_error);
        mListView.setAdapter(mAdapter);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void appendJourneys(ArrayList<Journey> journeys)
    {
        mSchedule.addAll(journeys);
        mAdapter.notifyDataSetChanged();

        mProgressBar.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
        mListView.setSelection(mAdapter.getNearest());
        Log.d(LOG_TAG, "schedule updated");
    }

    public void replaceJourneys(ArrayList<Journey> journeys)
    {
        // Notify about changes only when schedule differs to avoid
        // ListView flicker
        if (mSchedule.size() == journeys.size() && mSchedule.containsAll(journeys)) {
            Log.d(LOG_TAG, "schedule unchanged");
            return;
        }
        mSchedule.clear();
        appendJourneys(journeys);
    }

    public void showError(int resId) {
        mProgressBar.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.VISIBLE);
        mErrorTextView.setText(resId);
    }

}
