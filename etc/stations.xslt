<?xml version="1.0" encoding="utf-8"?>
<!--
    Elektrichki: suburban trains schedule for Android
    Copyright (C) 2015-2023  Andrew Nayenko

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<!--
     Usage:
        xsltproc stations.xslt index.xml > ../src/main/res/values/stations.xml
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" />

    <xsl:template match="/">
        <resources>
            <xsl:apply-templates select="/index" />
        </resources>
    </xsl:template>

    <xsl:template match="index">

        <string-array name="stations_array" translatable="false">
            <xsl:for-each select="document(url)//citystations/station">
                <xsl:sort select="@esr" data-type="number" />
                <item><xsl:value-of select="@title" /></item>
                <xsl:if test="@popular_title != ''">
                    <item><xsl:value-of select="@popular_title" /></item>
                </xsl:if>
            </xsl:for-each>
        </string-array>

        <string-array name="esr_array" translatable="false">
            <xsl:for-each select="document(url)//citystations/station">
                <xsl:sort select="@esr" data-type="number" />
                <item><xsl:value-of select="@esr" /></item>
                <xsl:if test="@popular_title != ''">
                    <item><xsl:value-of select="@esr" /></item>
                </xsl:if>
            </xsl:for-each>
        </string-array>

    </xsl:template>

</xsl:stylesheet>
